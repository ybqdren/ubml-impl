/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.spi.IMetadataReferenceManager;

public class ReferenceManagerHelper extends MetadataConfigurationLoader {
    private static ReferenceManagerHelper instance;

    public static ReferenceManagerHelper getInstance() {
        if (instance == null) {
            instance = new ReferenceManagerHelper();
        }
        return instance;
    }

    /// <summary>
    /// 构造器
    /// </summary>
    private ReferenceManagerHelper() {

    }

    /// <summary>
    /// 返回各元数据序列化器
    /// </summary>
    /// <param name="typeName"></param>
    /// <returns></returns>
    public IMetadataReferenceManager getManager(String typeName) {
        IMetadataReferenceManager manager = null;
        MetadataConfiguration data = getMetadataConfigurationData(typeName);
        if (data != null && data.getReference() != null) {
            Class<?> cls = null;
            try {
                cls = Class.forName(data.getReference().getName());
                manager = (IMetadataReferenceManager) cls.newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
