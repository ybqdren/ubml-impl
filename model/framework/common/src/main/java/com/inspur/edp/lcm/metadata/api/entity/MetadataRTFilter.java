/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity;

import java.util.List;

/**
 * Classname MetadataRTFilter Description 过滤条件实体类 Date 2019/11/18 15:21
 *
 * @author zhongchq
 * @version 1.0
 */
public class MetadataRTFilter {
    //关键应用编号
    public String appCode;
    //服务单元编号
    public String serviceUnitCode;
    //业务对象ID
    public List<String> bizobjectId;

    public String type;

    public String code;

    public String nameSpace;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getServiceUnitCode() {
        return serviceUnitCode;
    }

    public void setServiceUnitCode(String serviceUnitCode) {
        this.serviceUnitCode = serviceUnitCode;
    }

    public List<String> getBizobjectId() {
        return bizobjectId;
    }

    public void setBizobjectId(List<String> bizobjectId) {
        this.bizobjectId = bizobjectId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    @Override
    public String toString() {
        return "MetadataRTFilter{" +
            "appCode='" + appCode + '\'' +
            ", serviceUnitCode='" + serviceUnitCode + '\'' +
            ", bizobjectId=" + bizobjectId +
            ", type='" + type + '\'' +
            ", code='" + code + '\'' +
            ", nameSpace='" + nameSpace + '\'' +
            '}';
    }
}
