/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.entity.ProjectMetadataCache;
import com.inspur.edp.lcm.metadata.cache.MetadataDevCacheManager;
import com.inspur.edp.lcm.metadata.common.configuration.RefStrategyHelper;
import com.inspur.edp.lcm.metadata.core.index.MetadataPackageIndexServiceForMaven;
import com.inspur.edp.lcm.metadata.core.index.MetadataPackageIndexServiceForPackages;
import com.inspur.edp.lcm.metadata.core.index.ProjectMetadataCacheService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaoleitr
 */
public class RefCommonCoreService {
    private final MetadataCoreManager metadataManager = new MetadataCoreManager();
    private final MetadataProjectCoreService projectService = new MetadataProjectCoreService();
    private ProjectMetadataCacheService projectMetadataCacheService;

    public GspMetadata getRefMetadata(String metadataId, String path, String packagePath, String mavenPath) {

        // 工程信息
        String projectPath = projectService.getProjPath(path);
        MetadataProject metadataProject = projectService.getMetadataProjInfo(path);

        // 当前工程及bo下的引用的工程下搜索
        GspMetadata gspMetadata = getMetadataFromProjects(metadataId, metadataProject);
        if (gspMetadata != null) {
            return gspMetadata;
        }

        // 从缓存中获取
        projectMetadataCacheService = ProjectMetadataCacheService.getNewInstance(metadataProject, projectPath, packagePath, mavenPath);
        projectMetadataCacheService.setRefStrategy(RefStrategyHelper.getInstance().getRefStrategy());
        String metadataPackageLocation = getMetadataPackageLocationFromCache(metadataId, metadataProject, false, packagePath, mavenPath);
        if (metadataPackageLocation == null || metadataPackageLocation.isEmpty()) {
            metadataPackageLocation = getMetadataPackageLocationFromCache(metadataId, metadataProject, true, packagePath, mavenPath);
            if (metadataPackageLocation == null || metadataPackageLocation.isEmpty()) {
                metadataManager.throwMetadataNotFoundException(metadataId);
            }
        }
        File metadataPackage = new File(metadataPackageLocation);
        gspMetadata = metadataManager.getMetadataFromPackage(metadataPackage.getName(), metadataPackage.getParent(), metadataId).getMetadata();
        return gspMetadata;
    }

    private GspMetadata getMetadataFromProjects(String metadataID, MetadataProject metadataProject) {
        List<String> refProjPaths = new ArrayList<>();
        if (metadataProject != null) {
            refProjPaths.add(metadataProject.getProjectPath());
            projectService.getRefProjPaths(metadataProject.getProjectPath(), refProjPaths);

            for (String projPath : refProjPaths) {
                List<GspMetadata> metadataList = metadataManager.getMetadataList(projPath);
                for (GspMetadata gspMetadata : metadataList) {
                    if (gspMetadata.getHeader() != null && gspMetadata.getHeader().getId() != null && metadataID.equals(gspMetadata.getHeader().getId())) {
                        gspMetadata = metadataManager.loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath());
                        return gspMetadata;
                    }
                }
            }
        }
        return null;
    }

    private String getMetadataPackageLocationFromCache(String metadataID, MetadataProject metadataProject,
        Boolean refreshIndexFlag, String packagePath, String mavenPath) {
        if (refreshIndexFlag) {
            MetadataPackageIndexServiceForPackages.getInstance(packagePath).refreshMetadataPackageIndex();
            MetadataPackageIndexServiceForMaven.getInstance(mavenPath).refreshMetadataPackageIndex();
            projectMetadataCacheService.clearCache();
        }
        projectMetadataCacheService.syncProjectMetadataCache();
        ProjectMetadataCache projectMetadataCacheMap = (ProjectMetadataCache) MetadataDevCacheManager.getProjectMetadataCacheMap(metadataProject.getProjectPath());
        return projectMetadataCacheMap.getMetadataPackageLocations().get(metadataID);
    }
}
