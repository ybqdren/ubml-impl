/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.manager;

import com.inspur.edp.ide.setting.api.entity.MetadataIndexServer;
import com.inspur.edp.ide.setting.api.service.SettingService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class IndexServerManager {
    private SettingService settingService;

    private SettingService getSettingService() {
        if (settingService == null) {
            settingService = SpringBeanUtils.getBean(SettingService.class);
        }
        return settingService;
    }

    private static IndexServerManager instance;

    public static IndexServerManager getInstance() {
        if (instance == null) {
            instance = new IndexServerManager();
        }
        return instance;
    }

    public String getPageWebapiOfIndexServer() {
        return getIndexServerUrl() + "/page";
    }

    public String getPageWebapiOfIndexServerWithProcessMode() {
        return getIndexServerWithProcessModeUrl() + "/page";
    }

    public String getMetadataWebapiOfIndexServer() {
        return getIndexServerUrl() + "/metadataId";
    }

    public String getIndexServerUrl() {
        MetadataIndexServer metadataIndexServer = getSettingService().getMetadataIndexServer();
        return "http://" + metadataIndexServer.getIp() + ":" + metadataIndexServer.getPort() + "/api/v1/metadataindex/mvn";
    }

    public String getIndexServerWithProcessModeUrl() {
        return getIndexServerUrl() + "/withprocessmode";
    }

    public String getTestStatusWebapiOfIndexServer(String ip, String port) {
        return "http://" + ip + ":" + port + "/api/v1/metadataindex/mvn/metadataId?metadataId=test";
    }
}
