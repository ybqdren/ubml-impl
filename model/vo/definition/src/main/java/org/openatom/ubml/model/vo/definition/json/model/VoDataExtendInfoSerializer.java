/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;
import org.openatom.ubml.model.vo.definition.dataextendinfo.VoDataExtendInfo;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.operation.VmActionCollectionSerializer;

/**
 * The  Josn Serializer Of View Model Data Extendion Info
 *
 * @ClassName: VoDataExtendInfoSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoDataExtendInfoSerializer extends JsonSerializer<VoDataExtendInfo> {
    @Override
    public void serialize(VoDataExtendInfo info, JsonGenerator writer, SerializerProvider serializers) {
        if (info == null) {
            SerializerUtils.writeStartObject(writer);
            SerializerUtils.writeEndObject(writer);
            return;
        }
        //{
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataMappingActions);
        writeVmActions(writer, info.getDataMappingActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeQueryActions);
        writeVmActions(writer, info.getBeforeQueryActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.QueryActions);
        writeVmActions(writer, info.getQueryActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterQueryActions);
        writeVmActions(writer, info.getAfterQueryActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeRetrieveActions);
        writeVmActions(writer, info.getBeforeRetrieveActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.RetrieveActions);
        writeVmActions(writer, info.getRetrieveActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterRetrieveActions);
        writeVmActions(writer, info.getAfterRetrieveActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeModifyActions);
        writeVmActions(writer, info.getBeforeModifyActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ModifyActions);
        writeVmActions(writer, info.getModifyActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterModifyActions);
        writeVmActions(writer, info.getAfterModifyActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ChangesetMappingActions);
        writeVmActions(writer, info.getChangesetMappingActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeCreateActions);
        writeVmActions(writer, info.getBeforeCreateActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.CreateActions);
        writeVmActions(writer, info.getCreateActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterCreateActions);
        writeVmActions(writer, info.getAfterCreateActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeDeleteActions);
        writeVmActions(writer, info.getBeforeDeleteActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DeleteActions);
        writeVmActions(writer, info.getDeleteActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterDeleteActions);
        writeVmActions(writer, info.getAfterDeleteActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeSaveActions);
        writeVmActions(writer, info.getBeforeSaveActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataReversalMappingActions);
        writeVmActions(writer, info.getDataReversalMappingActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterSaveActions);
        writeVmActions(writer, info.getAfterSaveActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ChangesetReversalMappingActions);
        writeVmActions(writer, info.getChangesetReversalMappingActions());

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeMultiDeleteActions);
        writeVmActions(writer, info.getBeforeMultiDeleteActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.MultiDeleteActions);
        writeVmActions(writer, info.getMultiDeleteActions());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterMultiDeleteActions);
        writeVmActions(writer, info.getAfterMultiDeleteActions());
        //}
        SerializerUtils.writeEndObject(writer);
    }

    private void writeVmActions(JsonGenerator writer, VMActionCollection actions) {
        getVMActionCollectionConvertor().serialize(actions, writer, null);
    }

    private VmActionCollectionSerializer getVMActionCollectionConvertor() {
        return new VmActionCollectionSerializer();
    }
}
