/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.common;

/**
 * The Serializer Property Names Of Entity
 *
 * @ClassName: BizEntityJsonConst
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizEntityJsonConst {

    //业务实体
    public static final String CATEGORY = "Category";
    public static final String SOURCE_ENTITY = "SourceEntity";
    public static final String DATA_LOCK_TYPE = "DataLockType";
    public static final String EXTEND_TYPE = "ExtendType";
    public static final String DEPENDENT_ENTITY_ID = "DependentEntityId";
    public static final String DEPENDENT_ENTITY_NAME = "DependentEntityName";
    public static final String ENTITY_PACKAGE_NAME = "DependentEntityPackageName";
    public static final String IS_USING_TIME_STAMP = "IsUsingTimeStamp";
    public static final String BIZ_MGR_ACTIONS = "BizMgrActions";
    public static final String BIZ_MGR_ACTION = "BizMgrAction";
    public static final String COMPONENT_ASSEMBLY_NAME = "ComponentAssemblyName";
    public static final String ASSEMBLY_NAME = "AssemblyName";
    public static final String CACHE_CONFIGURATION = "CacheConfiguration";
    public static final String ENABLE_CACHING = "EnableCaching";
    public static final String ENABLE_TREE_DTM = "EnableTreeDtm";
    public static final String IS_DEFAULT_NULL = "IsDefaultNull";

    //业务实体对象
    public static final String DETERMINATIONS = "Determinations";
    public static final String VALIDATIONS = "Validations";
    public static final String BIZ_ACTIONS = "BizActions";
    public static final String BELONG_MODEL = "BelongModel";
    public static final String PARENT_OBJECT = "ParentObject";
    public static final String PARENT_OBJECT_ID = "ParentObjectID";
    public static final String LOGIC_DELETE_CONTROL_INFO = "LogicDeleteControlInfo";

    //业务实体字段
    /**
     *
     */
    public static final String CALCULATION_EXPRESS = "CalculationExpress";
    public static final String VALIDATION_EXPRESS = "ValidationExpress";
    public static final String ELEMENT_CONFIG_ID = "RtElementConfigId";
    public static final String READONLY = "Readonly";
    public static final String REQUIRED_CHECK_OCCASION = "RequiredCheckOccasion";
    public static final String UNIFIED_DATA_TYPE = "UnifiedDataType";

    /**
     * Operation
     */
    public static final String DESCRIPTION = "Description";
    public static final String COMPONENT_ID = "ComponentId";
    public static final String COMPONENT_PKG_NAME = "ComponentPkgName";
    public static final String COMPONENT_NAME = "ComponentName";
    public static final String IS_VISIBLE = "IsVisible";
    public static final String OP_TYPE = "OpType";
    public static final String OWNER = "Owner";
    public static final String BELONG_MODEL_ID = "BelongModelID";
    public static final String IS_REF = "IsRef";
    public static final String IS_GENERATE_COMPONENT = "IsGenerateComponent";

    /**
     * BizActionBase
     */
    public static final String CURENT_AUTH_TYPE = "CurentAuthType";
    public static final String PARAMETERS = "Parameters";
    public static final String ReturnValue = "ReturnValue";
    public static final String OP_ID_LIST = "OpIdList";
    public static final String PARAM_CODE = "ParamCode";
    public static final String PARAM_NAME = "ParamName";
    public static final String PARAMETER_TYPE = "ParameterType";
    public static final String COLLECTION_PARAMETER_TYPE = "CollectionParameterType";
    public static final String ASSEMBLY = "Assembly";
    public static final String CLASS_NAME = "ClassName";
    public static final String JAVA_CLASS_NAME = "JavaClassName";
    public static final String MODE = "Mode";
    public static final String PARAM_DESCRIPTION = "ParamDescription";

    /**
     * determination
     */
    public static final String DETERMINATION_TYPE = "DeterminationType";
    public static final String TRIGGER_TIME_POINT_TYPE = "TriggerTimePointType";
    public static final String REQUEST_NODE_TRIGGER_TYPE = "RequestNodeTriggerType";
    public static final String REQUEST_ELEMENTS = "RequestElements";
    public static final String REQUEST_CHILD_ELEMENTS = "RequestChildElements";
    public static final String REQUEST_CHILD_ELEMENT_KEY = "RequestChildElementKey";
    public static final String REQUEST_CHILD_ELEMENT_VALUE = "RequestChildElementValue";
    public static final String PRE_DTM_ID = "PreDtmId";
    public static final String DETERMINATION = "Determination";

    //validation
    public static final String VALIDATION_TYPE = "ValidationType";
    public static final String ORDER = "Order";
    public static final String PRECEDINGS = "Precedings";
    public static final String SUCCEEDINGS = "Succeedings";
    public static final String PRECEDING_IDS = "PrecedingIds";
    public static final String SUCCEEDING_IDS = "SucceedingIds";
    public static final String VALIDATION_TRIGGER_POINTS = "ValidationTriggerPoints";
    public static final String VALIDATION_TRIGGER_POINT_KEY = "ValidationTriggerPointKey";
    public static final String VALIDATION_TRIGGER_POINT_VALUE = "ValidationTriggerPointValue";
    public static final String VALIDATION_AFTER_SAVE = "ValAfterSave";
    public static final String VALIDATION = "Validation";

    /**
     * Authority
     */
    public static final String AUTHORIZATIONS = "Authorizations";
    public static final String AUTH_FIELD_INFOS = "AuthFieldInfos";
    public static final String AUTH_FIELD_ID = "AuthFieldID";
    public static final String AUTH_FIELD_NAME = "AuthFieldName";
    public static final String AUTHORIZATION_ID = "AuthorizationID";
    public static final String AUTH_ID = "AuthID";
    public static final String ELEMENT_ID = "ElementID";

    public static final String ACTION_INFOS = "ActionInfos";

    public static final String AUTH_ACTION_ID = "AuthActionID";
    public static final String AUTH_ACTION_OP_ID = "AuthActionOpID";
    public static final String AUTH_ACTION_OP_NAME = "AuthActionOpName";


    public static final String FUNC_OPERATION_ID = "FuncOperationID";
    public static final String FUNC_OPERATION_NAME = "FuncOperationName";

    /**
     * Increment
     */
    public static final String ADDED_ACTION = "AddedAction";
    public static final String MODIFY_ACTION = "ModifyAction";

}