/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.merger;

import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspEnumValueCollection;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.element.GspEnumValue;
import org.openatom.ubml.model.common.definition.cef.i18n.context.ICefResourceMergeContext;
import org.openatom.ubml.model.common.definition.cef.i18n.names.CefResourceKeyNames;
import org.openatom.ubml.model.common.definition.cef.increment.merger.MergeUtils;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

public abstract class CefFieldResourceMerger extends AbstractResourceMerger {

    private IGspCommonField field;

    protected CefFieldResourceMerger(IGspCommonField field,
        ICefResourceMergeContext context) {
        super(context);
        this.field = field;
    }

    @Override
    protected void mergeItems() {

        //关联枚举

        switch (field.getObjectType()) {
            case Association:
                mergeAssoInfo(field);
                break;
            case Enum:
                mergeEnum(field);
                break;
            case None:
                I18nResourceItemCollection resourceItems = getContext().getResourceItems();
                String keyPrefix = MergeUtils.getKeyPrefix(this.field.getI18nResourceInfoPrefix(), CefResourceKeyNames.NAME);
                field.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
                break;
            default:
                throw new RuntimeException("暂不支持" + field.getObjectType() + "类型字段的多语合并");
        }

        //扩展
        extractExtendProperties(field);
    }

    private void mergeEnum(IGspCommonField field) {
        I18nResourceItemCollection resourceItems = getContext().getResourceItems();
        String keyPrefix = MergeUtils
            .getKeyPrefix(this.field.getI18nResourceInfoPrefix(), CefResourceKeyNames.NAME);
        field.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
        GspEnumValueCollection enumValues = field.getContainEnumValues();
        if (enumValues != null && enumValues.size() > 0) {
            for (GspEnumValue item : enumValues) {
                String enumValueKeyPrefix = MergeUtils
                    .getKeyPrefix(item.getI18nResourceInfoPrefix(), CefResourceKeyNames.DISPLAY_VALUE);
                item.setName(resourceItems.getResourceItemByKey(enumValueKeyPrefix).getValue());

            }
        }
    }

    private void mergeAssoInfo(IGspCommonField field) {
        I18nResourceItemCollection resourceItems = getContext().getResourceItems();
        String keyPrefix = MergeUtils
            .getKeyPrefix(this.field.getI18nResourceInfoPrefix(), CefResourceKeyNames.NAME);
        field.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());

        GspAssociationCollection assos = field.getChildAssociations();
        if (assos != null && assos.size() > 0) {
            for (GspAssociation item : assos) {
                getAssoResourceMerger(getContext(), item).merge();
            }
        }
    }

    protected void extractExtendProperties(IGspCommonField commonField) {
    }

    protected abstract AssoResourceMerger getAssoResourceMerger(ICefResourceMergeContext context, GspAssociation asso);

}
